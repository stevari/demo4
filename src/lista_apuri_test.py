from cmath import log
from distutils.log import debug
import unittest

import lista_apuri

class Test_TestaaLaskin(unittest.TestCase):
    global lista
    lista =[1,2,3,4,5,6,7,8,9,10] 
    def testaa_parillisten_maara(self):
        tulos = lista_apuri.parillisten_maara(lista)
        self.assertEqual(tulos,5)
        #Tulos ennen korjausta oli 6, vaikka sen pitäisi olla 5.
        #Tämä johtui siitä että parillisten määrää kirjaa pitävä muuttuja n aloitusarvo oli 1,
        #vaikka sen pitäisi olla 0. Eihän parillisia voi olla ennen kuin tiedetään millainen lista
        #on kyseessä.
    def testaa_parittomien_maara(self):
        tulos = lista_apuri.parittomien_maara(lista)
        self.assertEqual(tulos,5)
        #Tulos ennen korjausta 4 vaikka sen pitäisi olla 5
        #funktion idea on palauttaa luku joka on koko listan pituus (10) - parillisten määrä, eli 5
        #mutta se palauttaa 4 loogisen virheen takia (parillisten määrä - 1)

    def testaa_negatiivisten_maara(self):
        lista.append(-1) #lisätään listaan negatiivinen alkio
        tulos = lista_apuri.negatiivisten_maara(lista)
        self.assertEqual(tulos,1)
        #Sama virhe kuin parillisten määrän testaamisessa, n = 1 vaikka sen pitäisi olla alussa 0
    